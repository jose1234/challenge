from API.config import (
    APP_URL,
    AUTH,
    SESSION)

class Product_Category:

    def __init__(self):
        self.category_url = "/products/categories"

    def _create_product_category(self, data):
        response = SESSION.post(f"{APP_URL}{self.category_url}", data,
        auth=AUTH)
        return response

    def _get_product_categories(self):
        response = SESSION.get(f"{APP_URL}{self.category_url}",
        auth=AUTH)
        return response
