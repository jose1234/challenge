from API.config import (
    APP_URL,
    AUTH,
    SESSION)

class Coupon:

    def __init__(self):
        self.coupon_url = "/coupons"

    def _create_coupon(self, data):
        response = SESSION.post(f"{APP_URL}{self.coupon_url}", data,
        auth=AUTH)
        return response
