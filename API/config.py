import requests
from requests.auth import HTTPBasicAuth
import logging
import os

SESSION = requests.Session()
APP_URL = os.getenv("APP_URL", "")
USERNAME = "shopmanager"
PASSWORD = "axY2 rimc SzO9 cobf AZBw NLnX"
AUTH = HTTPBasicAuth(USERNAME, PASSWORD)
LOG = logging.getLogger()

class HideSensitiveData(logging.Filter):

    def filter(self, record):
        record.msg = str(record.msg).replace(PASSWORD, "********")
        return True

LOG.addFilter(HideSensitiveData())
