from API.lib.coupon import Coupon
from API.config import LOG

def test_create_coupon():
    LOG.info("test_create_coupon() is executing")
    coupon_data = {
    "code": "jose",
    "discount_type": "percent",
    "amount": "10",
    "individual_use": True,
    "exclude_sale_items": True,
    "minimum_amount": "100.00"
    }
    response = Coupon()._create_coupon(coupon_data)
    LOG.debug(f"This coupon {response.json()} is successfully created")
    assert response.ok

if __name__ == '__main__':
    test_create_coupon()
