from API.lib.product_category import Product_Category
from API.config import LOG


def test_create_product_category():
    LOG.info("test_create_product_category() is executing")
    category_data = {
        "name": "Shorts",
        "description": "You will have all shorts products here"
    }
    response = Product_Category()._create_product_category(category_data)
    LOG.debug(f"This product category {response.json()} \
        is successfully created")
    assert response.ok


def test_get_product_categories():
    LOG.info("test_get_product_categories() is executing")
    response = Product_Category()._get_product_categories()
    LOG.debug(f"The product categories are {response.json()}")
    assert response.ok


if __name__ == '__main__':
    test_create_product_category()
