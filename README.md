# QA Automation Challenge

This project consists of two parts:
1. Challenge part #1:
- Using the testing playground, come up with 6 manual test cases 3 test cases for the UI.
- And another 3 for the API (Rest API WooCommerce)
All of this first section are located in the Test_Cases folder inside you can find one document named: ChallengeTC.doc
2. Challenge part #2:
- Automate the 3 manual UI test cases using Cypress (the ones I wrote in part #1) located in the UI folder.
- Automate the other 3 manual API tests cases using Python with Pytest (the ones I wrote in part #1) located in the API folder.
### All of them automate the playground web page: http://34.205.174.166/

## Prerequisites
- Git
- Cypress package version: 7.7.0
- Nodejs package version: 14.17.1
- Npm package version: 6.14.13
- Python 3.8 and up
- Any source code editor tool like Visual Studio Code

## Setup for the API project
1. Open a terminal
2. Clone the repository
```
git clone https://gitlab.com/jose1234/challenge.git
```
3. Create a new file inside of API folder, name it secrets.env and add the following contents:
```
PYTHONPATH=$(pwd)
APP_URL=http://34.205.174.166/wp-json/wc/v3
```
4. Run this command to install the libraries
```
pip3 install -r requirements.txt
```
5. Start development environment
```
sh API/suites/all_tests.sh
```
With this command you will run all the test suite that I have in the project, the suite has three test cases.
And all the reports you can find it: API/reports/all_tests.html

## Setup for the UI project
1. Remember you already have the UI project because you cloned in the setup for the API so verify if you have the cypress project inside the UI folder.
2. Open a terminal
3. Install the dependencies for UI project
```
cd UI
npm install
```
4. Install Cypress framework
```
npm install cypress --save-dev
```
5. Open Cypress inside the UI folder
```
npx cypress open
```
6. Run Tests inside the UI folder
```
npm run cypress:run
```

And all the reports you can find it: UI/mochawesome-report/mochawesome.html open the html using your preference browser
