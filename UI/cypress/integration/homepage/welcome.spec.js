/// <reference types="cypress" />
describe('Test suite for homepage', () => {
    beforeEach('open the application', () => {
        cy.openHomePage()
    })

    it('should have the welcome message' , () => {
        cy.get('.wp-block-cover__inner-container')
        .contains('Welcome')
        .should('have.attr', 'style', 'text-align:center')
    })
})
