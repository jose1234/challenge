/// <reference types="cypress" />

import { navigateTo } from '../../support/page_objects/navigationPage'

describe('Test suite for account', () => {
    beforeEach('open the application', () => {
        cy.openHomePage()
    })

    it('should have a My Account title', () =>{
        navigateTo.accountsPage()
        cy.title().should('eq', 'My account – QA Playground')
})
})
