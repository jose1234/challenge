

export class NavigationPage{

  accountsPage(){
    cy.get('#menu-primary-menu')
    .contains('a','My account')
    .click()
  }

}

export const navigateTo = new NavigationPage()
